#!/usr/bin/env python
# Code by: Daniël Stekelenburg
import socket
import time
import sys
import thread
import threading
import sys
import urllib
import os
from threading import Thread
import math
import random
import pigpio

pi = pigpio.pi()

motor_powerrate = 0.20
clientactive = 0

Flash  = 3
Servo = 26
MotorLa = 14
MotorLb = 15
MotorRa = 23
MotorRb = 24

pi.set_mode(Flash, pigpio.OUTPUT) 
pi.set_mode(Servo, pigpio.OUTPUT) 

pi.set_mode(MotorLa, pigpio.OUTPUT) 
pi.set_mode(MotorLb, pigpio.OUTPUT) 
pi.set_mode(MotorRa, pigpio.OUTPUT) 
pi.set_mode(MotorRb, pigpio.OUTPUT) 

pi.set_PWM_range(MotorLa, 100)  	
pi.set_PWM_range(MotorLb, 100)  	
pi.set_PWM_range(MotorRa, 100)  	
pi.set_PWM_range(MotorRb, 100)  	

pi.set_PWM_frequency(MotorLa, 500)	
pi.set_PWM_frequency(MotorLb, 500)	
pi.set_PWM_frequency(MotorRa, 500)	
pi.set_PWM_frequency(MotorRb, 500)	

def SetServo(angle):
	calc1  = ((float(angle)/120) * 2000 + 500)
	pi.set_servo_pulsewidth(Servo, calc1)
def restart_program():
	Shutdown()
	python = sys.executable
	os.execl(python, python, * sys.argv)
def Reset():
	SetServo(60)
	pi.write(Flash,1)
	pi.set_PWM_dutycycle(MotorLa,0)
	pi.set_PWM_dutycycle(MotorLb,0)
	pi.set_PWM_dutycycle(MotorRa,0)
	pi.set_PWM_dutycycle(MotorRb,0)
def Shutdown():
	pi.set_servo_pulsewidth(Servo, 0)
	pi.write(Flash,0)
	pi.set_PWM_dutycycle(MotorLa,0)
	pi.set_PWM_dutycycle(MotorLb,0)
	pi.set_PWM_dutycycle(MotorRa,0)
	pi.set_PWM_dutycycle(MotorRb,0)
	pi.stop()
	sock.close()
def BG():
	global clientactive
	global pingtime
	while 1:
			if (clientactive == 0):
				print("Motors have been shutdown due to inactivity")
				Reset()
			else:
				print("Client still active")
			clientactive = 0
			time.sleep(2)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.bind(('', 1234))
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

Reset()

thread.start_new_thread(BG, ())

while 1:
	try:
		data, addr = sock.recvfrom(10)
		split =  data.decode('utf-8').split(":");
		command = split[0];
		message = split[1];
		if not command:
			break
		if command == '1':
			SetServo(120 - int(message))
			print("SERVO ANGLE: " + message)
		elif command == '2':
			brutomessage = float(message) - 100
			if brutomessage > 0:
				power = int(brutomessage * motor_powerrate)
				print("Motor LEFT: FORWARD > " + str(power))
				pi.set_PWM_dutycycle(MotorLb,0)
				pi.set_PWM_dutycycle(MotorLa,power)
			elif brutomessage < 0:
				power = int(brutomessage * -1 * motor_powerrate)
				print("Motor LEFT: BACKWARD > " + str(power))
				pi.set_PWM_dutycycle(MotorLa,0)
				pi.set_PWM_dutycycle(MotorLb,power)
			else:
				print('Motor LEFT: NEUTRAL')
				pi.set_PWM_dutycycle(MotorLa,0)
				pi.set_PWM_dutycycle(MotorLb,0)
		elif command == '3':
			brutomessage = float(message) - 100
			if brutomessage > 0:
				power = int(brutomessage * motor_powerrate)
				print("Motor RIGHT: FORWARD > " + str(power))
				pi.set_PWM_dutycycle(MotorRb,0)
				pi.set_PWM_dutycycle(MotorRa,power)
			elif brutomessage < 0:
				power = int(brutomessage * -1 * motor_powerrate)
				print("Motor RIGHT: BACKWARD > " + str(power))
				pi.set_PWM_dutycycle(MotorRa,0)
				pi.set_PWM_dutycycle(MotorRb,power)
			else:
				print('Motor RIGHT: NEUTRAL')
				pi.set_PWM_dutycycle(MotorRa,0)
				pi.set_PWM_dutycycle(MotorRb,0)
		elif command == '4':
			if str(message) == 'true':
				print("Flashlight: ON")
				pi.write(Flash,0)
			else:
				print("Flashlight: OFF")
				pi.write(Flash,1)
		elif command == "5":
			if motor_powerrate == 0.20:
				print("FULL POWER: ON")
				motor_powerrate = 1.00
			else:
				print("FULL POWER: OFF")
				motor_powerrate = 0.20
		elif command == "6":
			print("RESTART")
			restart_program()
		elif command == "7":
			print("UPDATE")
			urllib.urlretrieve("http://daniekq114.114.axc.nl/main.py","main.py")
			restart_program()
		elif command == "8":
			print("SHUTDOWN")
			Shutdown()
			time.sleep(1)
			os.system("sudo shutdown -P now")
		elif command == "9":
			if str(message) == "ping":
				clientactive = 1
	except KeyboardInterrupt:
		Reset()
		pi.stop()
		break
	except Exception as e:
		print("Error: ")
		print (sys.exc_traceback.tb_lineno)
		print (str(e))
		Reset()
		pi.stop()
		time.sleep(1)
