package stekelenburg.daniel.prof_werkstuk;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;
public class Main extends Activity {
    NetworkMGR mgr = new NetworkMGR();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText txtUrl = new EditText(this);
        txtUrl.setInputType(InputType.TYPE_CLASS_NUMBER);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setProgressDrawable(getResources()
                .getDrawable(R.drawable.progressbar));
        SeekBar seekBar1 = (SeekBar) findViewById(R.id.seekBar1);
        seekBar1.setProgressDrawable(getResources()
                .getDrawable(R.drawable.progressbar));
        SeekBar seekBar2 = (SeekBar) findViewById(R.id.seekBar2);
        seekBar2.setProgressDrawable(getResources()
                .getDrawable(R.drawable.progressbar));
        final Button OPT = (Button) findViewById(R.id.button3);
        final TextView timer = (TextView) findViewById(R.id.textView);
        final TextView queuesize = (TextView) findViewById(R.id.textView2);
        final WebView web = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = web.getSettings();
        webSettings.setJavaScriptEnabled(true);
        web.loadUrl("http://192.168.43.123:8080/?action=stream");
        Runnable r1 = new Runnable() {
            public void run() {
                try {
                    while (true) {
                        Command com = new Command();
                        com.command = "9";
                        com.message = "ping";
                        try {
                            mgr.queue.put(com);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(r1).start();
        Runnable r2 = new Runnable() {
            public void run() {
                try {
                    while (true) {
                        if (mgr.queue.size() > 0) {
                            Main.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    timer.setTextColor(Color.WHITE);
                                }
                            });
                            Command gotten = mgr.queue.take();
                            final long now = System.currentTimeMillis();
                            final int answer = mgr.SendCommand(gotten);
                            final long deltat = System.currentTimeMillis() - now;
                            Main.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    if (answer == 1) {
                                        timer.setTextColor(Color.GREEN);
                                    } else {
                                        timer.setTextColor(Color.RED);
                                    }
                                    timer.setText("Time: " + String.valueOf(deltat));
                                    queuesize.setText("Queue: " + String.valueOf(mgr.queue.size()));
                                }
                            });
                        } else {
                            Thread.sleep(10);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(r2).start();
        String Options[] = {
                "Toggle Power",
                "Restart Program",
                "Update Program",
                "Shutdown",
                "Reload CAM"
        };
        final AlertDialog.Builder builder = new AlertDialog.Builder(Main.this);
        builder.setTitle("Options")
                .setItems(Options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Command com = new Command();
                        com.message = "1";
                        if (which == 0) {
                            com.command = "5";
                        } else if (which == 1) {
                            com.command = "6";
                        } else if (which == 2) {
                            com.command = "7";
                        } else if (which == 3) {
                            com.command = "8";
                        } else if (which == 4) {
                            web.loadUrl("http://192.168.43.123:8080/?action=stream");
                        }
                        try {
                            mgr.queue.put(com);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
        builder.create();
        OPT.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                builder.show();
            }
        });
        final ToggleButton light = (ToggleButton) findViewById(R.id.toggleButton);
        light.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Command com = new Command();
                com.command = "4";
                if (light.isChecked()) {
                    com.message = "true";
                } else {
                    com.message = "false";
                }
                try {
                    mgr.queue.put(com);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        final SeekBar servo = (SeekBar) findViewById(R.id.seekBar);
        servo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Command com = new Command();
                com.command = "1";
                com.message = String.valueOf(progress);
                try {
                    mgr.queue.put(com);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        final VerticalSeekBar motorLeft = (VerticalSeekBar) findViewById(R.id.seekBar1);
        motorLeft.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Command com = new Command();
                com.command = "2";
                com.message = String.valueOf(progress);
                try {
                    mgr.queue.put(com);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        final VerticalSeekBar motorRight = (VerticalSeekBar) findViewById(R.id.seekBar2);
        motorRight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Command com = new Command();
                com.command = "3";
                com.message = String.valueOf(progress);
                try {
                    mgr.queue.put(com);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        final Button Leftzero = (Button) findViewById(R.id.button);
        Leftzero.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                motorLeft.setProgress(100);
                motorLeft.updateThumb();
            }
        });
        final Button Rightzero = (Button) findViewById(R.id.button2);
        Rightzero.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                motorRight.setProgress(100);
                motorRight.updateThumb();
            }
        });
        final Button servozero = (Button) findViewById(R.id.button4);
        servozero.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                servo.setProgress(60);
            }
        });
    }
}