package stekelenburg.daniel.prof_werkstuk;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;
/**
 * Created by Daniel on 02-Dec-15.
 */
public class NetworkMGR {
    public String IP = "192.168.43.123";
    DatagramSocket s = null;
    InetAddress local = null;
    public NetworkMGR() {
        try {
            s = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        try {
            local = InetAddress.getByName(IP);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
    public int SendCommand(Command command) {
        String mes = command.command + ":" + command.message;
        int msg_length = mes.length();
        byte[] message = mes.getBytes();
        DatagramPacket p = new DatagramPacket(message, msg_length, local, 1234);
        try {
            s.send(p);
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }
    LinkedBlockingQueue < Command > queue = new LinkedBlockingQueue < Command > ();
}